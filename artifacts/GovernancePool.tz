{ parameter
    (or (or (or %admin (or (unit %confirm_admin) (bool %pause)) (address %set_admin))
            (nat %change_reward_rate))
        (or (nat %deposit) (nat %withdraw))) ;
  storage
    (pair (address %token_contract)
          (address %votes_contract)
          (pair %admin (pair (address %admin) (bool %paused)) (option %pending_admin address))
          (nat %pool_size)
          (timestamp %last_accrued_rewards_at)
          (nat %reward_rate)
          (nat %votes_minted)
          (big_map %metadata string bytes)) ;
  code { LAMBDA
           (pair (pair address bool) (option address))
           unit
           { CAR ;
             CAR ;
             SENDER ;
             COMPARE ;
             NEQ ;
             IF { PUSH string "NOT_AN_ADMIN" ; FAILWITH } { UNIT } } ;
         LAMBDA
           (pair (pair address bool) (option address))
           unit
           { CAR ; CDR ; IF { PUSH string "PAUSED" ; FAILWITH } { UNIT } } ;
         LAMBDA
           int
           nat
           { ISNAT ; IF_NONE { PUSH string "NOT_A_NAT" ; FAILWITH } {} } ;
         LAMBDA
           (pair (pair address address) address nat)
           operation
           { UNPAIR ;
             UNPAIR ;
             DIG 2 ;
             UNPAIR ;
             DIG 2 ;
             CONTRACT %transfer
               (list (pair (address %from_) (list %txs (pair (address %to_) (nat %token_id) (nat %amount))))) ;
             IF_NONE { PUSH string "MISSING_ENTRYPOINT" ; FAILWITH } {} ;
             PUSH mutez 0 ;
             NIL (pair address (list (pair address nat nat))) ;
             NIL (pair address nat nat) ;
             DIG 5 ;
             PUSH nat 0 ;
             DIG 6 ;
             PAIR 3 ;
             CONS ;
             DIG 4 ;
             PAIR ;
             CONS ;
             TRANSFER_TOKENS } ;
         LAMBDA
           (pair (lambda int nat)
                 (pair address
                       address
                       (pair (pair address bool) (option address))
                       nat
                       timestamp
                       nat
                       nat
                       (big_map string bytes)))
           (pair address
                 address
                 (pair (pair address bool) (option address))
                 nat
                 timestamp
                 nat
                 nat
                 (big_map string bytes))
           { UNPAIR ;
             SWAP ;
             DUP ;
             NOW ;
             UPDATE 9 ;
             PUSH nat 1000000000 ;
             DUP 3 ;
             GET 9 ;
             NOW ;
             SUB ;
             DIG 4 ;
             SWAP ;
             EXEC ;
             DUP 4 ;
             GET 11 ;
             MUL ;
             EDIV ;
             IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
             CAR ;
             DIG 2 ;
             GET 7 ;
             ADD ;
             UPDATE 7 } ;
         DUP 3 ;
         APPLY ;
         DIG 5 ;
         UNPAIR ;
         PUSH mutez 0 ;
         AMOUNT ;
         COMPARE ;
         GT ;
         IF { PUSH string "AMOUNT_NOT_ZERO" ; FAILWITH } {} ;
         IF_LEFT
           { DIG 3 ;
             DIG 4 ;
             DIG 5 ;
             DROP 3 ;
             IF_LEFT
               { DIG 2 ;
                 DROP ;
                 DUP 2 ;
                 DIG 2 ;
                 GET 5 ;
                 DIG 2 ;
                 IF_LEFT
                   { IF_LEFT
                       { DIG 3 ;
                         DROP 2 ;
                         DUP ;
                         CDR ;
                         IF_NONE
                           { DROP ; PUSH string "NO_PENDING_ADMIN" ; FAILWITH }
                           { SENDER ;
                             SWAP ;
                             DUP 2 ;
                             COMPARE ;
                             EQ ;
                             IF { NONE address ; DIG 2 ; CAR ; CDR ; DIG 2 ; PAIR ; PAIR }
                                { DROP 2 ; PUSH string "NOT_A_PENDING_ADMIN" ; FAILWITH } } }
                       { DUP 2 ;
                         DIG 4 ;
                         SWAP ;
                         EXEC ;
                         DROP ;
                         DUP 2 ;
                         CDR ;
                         SWAP ;
                         DIG 2 ;
                         CAR ;
                         CAR ;
                         PAIR ;
                         PAIR } }
                   { DUP 2 ; DIG 4 ; SWAP ; EXEC ; DROP ; SOME ; SWAP ; CAR ; PAIR } ;
                 UPDATE 5 }
               { DUP 2 ;
                 GET 5 ;
                 DIG 4 ;
                 SWAP ;
                 EXEC ;
                 DROP ;
                 SWAP ;
                 DIG 2 ;
                 SWAP ;
                 EXEC ;
                 SWAP ;
                 UPDATE 11 } ;
             NIL operation }
           { DIG 6 ;
             DROP ;
             IF_LEFT
               { DIG 2 ;
                 DROP ;
                 DUP 2 ;
                 GET 5 ;
                 DIG 5 ;
                 SWAP ;
                 EXEC ;
                 DROP ;
                 PUSH nat 1000000000 ;
                 DUP 3 ;
                 GET 9 ;
                 NOW ;
                 SUB ;
                 DIG 5 ;
                 SWAP ;
                 EXEC ;
                 DUP 4 ;
                 GET 11 ;
                 MUL ;
                 EDIV ;
                 IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                 CAR ;
                 DUP 3 ;
                 GET 7 ;
                 ADD ;
                 DUP 3 ;
                 GET 13 ;
                 DUP 3 ;
                 MUL ;
                 EDIV ;
                 IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                 CAR ;
                 DUP 2 ;
                 SELF_ADDRESS ;
                 PAIR ;
                 SENDER ;
                 DUP 5 ;
                 CAR ;
                 PAIR ;
                 PAIR ;
                 DUP 5 ;
                 SWAP ;
                 EXEC ;
                 DUP 4 ;
                 GET 3 ;
                 CONTRACT %mint_tokens (list (pair (address %owner) (nat %amount))) ;
                 IF_NONE { PUSH string "MISSING_ENTRYPOINT" ; FAILWITH } {} ;
                 NIL (pair address nat) ;
                 DUP 4 ;
                 SELF_ADDRESS ;
                 PAIR ;
                 CONS ;
                 SWAP ;
                 PUSH mutez 0 ;
                 DIG 2 ;
                 TRANSFER_TOKENS ;
                 DUP 3 ;
                 SENDER ;
                 PAIR ;
                 SELF_ADDRESS ;
                 DUP 7 ;
                 GET 3 ;
                 PAIR ;
                 PAIR ;
                 DIG 6 ;
                 SWAP ;
                 EXEC ;
                 DUP 6 ;
                 DIG 5 ;
                 DUP 7 ;
                 GET 7 ;
                 ADD ;
                 UPDATE 7 ;
                 DIG 4 ;
                 DIG 5 ;
                 GET 13 ;
                 ADD ;
                 UPDATE 13 }
               { DUP 2 ;
                 GET 5 ;
                 DIG 6 ;
                 SWAP ;
                 EXEC ;
                 DROP ;
                 SWAP ;
                 DIG 2 ;
                 SWAP ;
                 EXEC ;
                 DUP ;
                 GET 13 ;
                 DUP 2 ;
                 GET 7 ;
                 DUP 4 ;
                 MUL ;
                 EDIV ;
                 IF_NONE { PUSH string "DIV by 0" ; FAILWITH } {} ;
                 CAR ;
                 DUP 3 ;
                 SELF_ADDRESS ;
                 PAIR ;
                 SENDER ;
                 DUP 4 ;
                 GET 3 ;
                 PAIR ;
                 PAIR ;
                 DUP 5 ;
                 SWAP ;
                 EXEC ;
                 DUP 3 ;
                 GET 3 ;
                 CONTRACT %burn_tokens (list (pair (address %owner) (nat %amount))) ;
                 IF_NONE { PUSH string "MISSING_ENTRYPOINT" ; FAILWITH } {} ;
                 NIL (pair address nat) ;
                 DUP 6 ;
                 SELF_ADDRESS ;
                 PAIR ;
                 CONS ;
                 SWAP ;
                 PUSH mutez 0 ;
                 DIG 2 ;
                 TRANSFER_TOKENS ;
                 DUP 3 ;
                 SENDER ;
                 PAIR ;
                 SELF_ADDRESS ;
                 DUP 6 ;
                 CAR ;
                 PAIR ;
                 PAIR ;
                 DIG 6 ;
                 SWAP ;
                 EXEC ;
                 DUP 5 ;
                 DIG 4 ;
                 DUP 6 ;
                 GET 7 ;
                 SUB ;
                 DUP 8 ;
                 SWAP ;
                 EXEC ;
                 UPDATE 7 ;
                 DIG 5 ;
                 DIG 5 ;
                 GET 13 ;
                 SUB ;
                 DIG 5 ;
                 SWAP ;
                 EXEC ;
                 UPDATE 13 } ;
             NIL operation ;
             DIG 2 ;
             CONS ;
             DIG 2 ;
             CONS ;
             DIG 2 ;
             CONS } ;
         PAIR } }

