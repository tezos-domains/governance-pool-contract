type tzip12_transfer_destination = [@layout:comb] {
  to_: address;
  token_id: nat;
  amount: nat;
}

type tzip12_transfer = [@layout:comb] {
  from_: address;
  txs: tzip12_transfer_destination list;
}

type tzip12_transfer_param = tzip12_transfer list

type mint_burn_tx =
[@layout:comb]
{
  owner : address;
  amount : nat;
}

type mint_burn_tokens_param = mint_burn_tx list
