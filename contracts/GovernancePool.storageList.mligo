#include "GovernancePool.mligo"

// Define your initial storage values as a list of LIGO variable definitions,
// the first of which will be considered the default value to be used for origination later on
// E.g. let aStorageValue : aStorageType = 10

let default : storage = {
    token_contract = ("KT1VHyQPyaqvRuyv2cAmQi1zRNrgqn99dLwp" : address);
    votes_contract = ("KT1RZHHNeNbTo2a8j16TMZFHr6abTdaihiFy" : address);
    admin = {
        admin = ("tz1QGcAWeCTih1t7YGiUKmPdVMvRQpXv9d8A" : address);
        pending_admin = (None : address option);
        paused = true;
    };
    pool_size = 1n;
    last_accrued_rewards_at = (1678223768 : timestamp);
    reward_rate = 0n;
    votes_minted = 1n;
    metadata = Big_map.literal [
    ];
}

let ghostnet : storage = {
    token_contract = ("KT1Cu85fMDkDJLmKR1dya1kviuQWSXC2eHXp" : address);
    votes_contract = ("KT1URzN3tCnfUpGDenhogx4ppZCH1z8NWqWW" : address);
    admin = {
        admin = ("tz1Vi8wpXhuNibdPcZ3BVnBv2XUwamFEGoX7" : address);
        pending_admin = (None : address option);
        paused = true;
    };
    pool_size = 1n;
    last_accrued_rewards_at = (1693400857 : timestamp);
    reward_rate = 0n;
    votes_minted = 1n;
    metadata = Big_map.literal [
        // tezos-storage:content
        ("", 0x74657a6f732d73746f726167653a636f6e74656e74);
        // {"name":"Tezos Domains Governance Pool","version":"v1.0.0","license":{"name":"MIT","details":"MIT License"},"homepage":"https://tezos.domains/"}
        ("content", 0x7B226E616D65223A2254657A6F7320446F6D61696E7320476F7665726E616E636520506F6F6C222C2276657273696F6E223A2276312E302E30222C226C6963656E7365223A7B226E616D65223A224D4954222C2264657461696C73223A224D4954204C6963656E7365227D2C22686F6D6570616765223A2268747470733A2F2F74657A6F732E646F6D61696E732F227D)
    ];
}

let mainnet : storage = {
    token_contract = ("KT1GY5qCWwmESfTv9dgjYyTYs2T5XGDSvRp1" : address);
    votes_contract = ("KT1R4KPQxpFHAkX8MKCFmdoiqTaNSSpnJXPL" : address);
    admin = {
        admin = ("tz1Vi8wpXhuNibdPcZ3BVnBv2XUwamFEGoX7" : address);
        pending_admin = (None : address option);
        paused = true;
    };
    pool_size = 1n;
    last_accrued_rewards_at = (1693400857 : timestamp);
    reward_rate = 0n;
    votes_minted = 1n;
    metadata = Big_map.literal [
        // tezos-storage:content
        ("", 0x74657a6f732d73746f726167653a636f6e74656e74);
        // {"name":"Tezos Domains Governance Pool","version":"v1.0.0","license":{"name":"MIT","details":"MIT License"},"homepage":"https://tezos.domains/"}
        ("content", 0x7B226E616D65223A2254657A6F7320446F6D61696E7320476F7665726E616E636520506F6F6C222C2276657273696F6E223A2276312E302E30222C226C6963656E7365223A7B226E616D65223A224D4954222C2264657461696C73223A224D4954204C6963656E7365227D2C22686F6D6570616765223A2268747470733A2F2F74657A6F732E646F6D61696E732F227D)
    ];
}