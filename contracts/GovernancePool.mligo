#include "simple_admin.mligo"
#include "common.mligo"

type storage = [@layout:comb] {
    // primary token
    token_contract: address;

    // vote token
    votes_contract: address;
    
    // admin module
    admin: simple_admin_storage;

    // the amount of tokens in the pool (excluding pending rewards)
    pool_size: nat;

    // the timestamp of the last time rewards were accrued
    last_accrued_rewards_at: timestamp;

    // reward supply rate in seconds, with 1^e9 representing the token amount of 1 per second
    reward_rate: nat; 

    // the circulating supply of votes
    votes_minted: nat;

    // tzip-16 metadata
    metadata: (string, bytes) big_map;
}

let token_id = 0n

type return = (operation list) * storage

let assert_zero_amount (_ : unit) : unit = 
    if Tezos.get_amount() > 0tez then
        (failwith "AMOUNT_NOT_ZERO" : unit)
    else ()

let assert_nat (i : int) : nat =
  match is_nat(i) with
    | Some n -> n
    | None -> (failwith "NOT_A_NAT" : nat)

let transfer_tokens(contract_address, from, to, amount: address * address * address * nat) : operation = 
    let token_contract: tzip12_transfer_param contract =
        match (Tezos.get_entrypoint_opt "%transfer" contract_address : tzip12_transfer_param contract option) with
        | None -> (failwith "MISSING_ENTRYPOINT" : tzip12_transfer_param contract)
        | Some contract -> contract in
    let transfer_param: tzip12_transfer_param = [{from_ = from; txs = [{to_ = to; token_id = token_id; amount = amount}]}] in

    Tezos.transaction transfer_param 0mutez token_contract

[@inline]
let mint_or_burn (token_contract, amount: mint_burn_tokens_param contract * nat) : operation = 
    let mint_param: mint_burn_tokens_param = [{owner = Tezos.get_self_address(); amount = amount}] in
    Tezos.transaction mint_param 0mutez token_contract

[@inline]
let pending_rewards (store : storage) : nat =
    (store.reward_rate * assert_nat (Tezos.get_now() - store.last_accrued_rewards_at)) / 1_000_000_000n

[@inline]
let total_pool_size (store : storage) : nat = 
    store.pool_size + pending_rewards store

let mint_votes (votes_contract, amount: address * nat) : operation = 
    let token_contract: mint_burn_tokens_param contract =
        match (Tezos.get_entrypoint_opt "%mint_tokens" votes_contract : mint_burn_tokens_param contract option) with
        | None -> (failwith "MISSING_ENTRYPOINT" : mint_burn_tokens_param contract)
        | Some contract -> contract in
    mint_or_burn (token_contract, amount)

let burn_votes (votes_contract, amount: address * nat) : operation = 
    let token_contract: mint_burn_tokens_param contract =
        match (Tezos.get_entrypoint_opt "%burn_tokens" votes_contract : mint_burn_tokens_param contract option) with
        | None -> (failwith "MISSING_ENTRYPOINT" : mint_burn_tokens_param contract)
        | Some contract -> contract in
    mint_or_burn (token_contract, amount)

let accrue_rewards (store: storage): storage =
    {store with last_accrued_rewards_at = Tezos.get_now(); pool_size = store.pool_size + pending_rewards store }

let deposit (amount, store: nat * storage): return =
    let _ = fail_if_paused store.admin in

    let votes_minted = amount * store.votes_minted / (total_pool_size store) in

    let transfer_tokens_op = transfer_tokens(store.token_contract, Tezos.get_sender(), Tezos.get_self_address(), amount) in
    let mint_votes_op = mint_votes (store.votes_contract, votes_minted) in
    let transfer_votes_op = transfer_tokens(store.votes_contract, Tezos.get_self_address(), Tezos.get_sender(), votes_minted) in

    let store = {store with pool_size = store.pool_size + amount; votes_minted = store.votes_minted + votes_minted} in

    ([transfer_tokens_op; mint_votes_op; transfer_votes_op] : operation list), store

let withdraw (votes_amount, store: nat * storage): return =
    let _ = fail_if_paused store.admin in

    let store = accrue_rewards store in
    let tokens_transferred = votes_amount * store.pool_size / store.votes_minted in

    let transfer_votes_op = transfer_tokens(store.votes_contract, Tezos.get_sender(), Tezos.get_self_address(), votes_amount) in
    let burn_votes_op = burn_votes (store.votes_contract, votes_amount) in
    let transfer_tokens_op = transfer_tokens(store.token_contract, Tezos.get_self_address(), Tezos.get_sender(), tokens_transferred) in

    let store = {store with pool_size = assert_nat (store.pool_size - tokens_transferred); votes_minted = assert_nat (store.votes_minted - votes_amount)} in

    ([transfer_votes_op; burn_votes_op; transfer_tokens_op] : operation list), store

let change_reward_rate (new_supply_rate, store: nat * storage): return =
    let _ = fail_if_not_admin store.admin in
    
    let store = accrue_rewards store in
    let store = {store with reward_rate = new_supply_rate } in
    
    ([] : operation list), store

type parameter =
    Withdraw of nat
    | Deposit of nat
    | Change_reward_rate of nat
    | Admin of simple_admin

let main (action, store : parameter * storage) : return =
    let _ = assert_zero_amount () in
    match action with
        | Withdraw p -> withdraw (p, store)
        | Deposit p -> deposit (p, store)
        | Change_reward_rate p -> change_reward_rate (p, store)
        | Admin p -> ([] : operation list), { store with admin = simple_admin (p, store.admin) }
