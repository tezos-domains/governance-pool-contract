#include "./common.mligo"

type parameter =
    | Transfer of tzip12_transfer_param
    | Mint_tokens of mint_burn_tokens_param
    | Burn_tokens of mint_burn_tokens_param

type storage = parameter list

type return = operation list * storage

let main (action, store : parameter * storage) : return =
    ([] : operation list), action :: store
