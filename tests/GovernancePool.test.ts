import { ContractAbstraction, ContractProvider, TezosToolkit, MichelsonMap, SendParams, TransactionOperation, ContractMethod } from '@taquito/taquito';
import { BigNumber } from 'bignumber.js';
import { originateContract, signWithWallet } from './originate-contract';

import { getTaqueriaConfig } from './taqueria-config';

export const defaultSendParams: SendParams = {
    amount: 0,
    storageLimit: 1_000
};


describe('GovernancePool contract', () => {
    let Tezos: TezosToolkit;

    const { rpcUrl, accounts: { alice, bob, jane } } = getTaqueriaConfig();

    let contractAddress: string;
    let votesContractAddress: string;
    let tokenContractAddress: string;

    jest.setTimeout(50000);

    beforeAll(async () => {
        Tezos = new TezosToolkit(rpcUrl);
    });

    async function getTezosTime(block?: number): Promise<number> {
        let header = await Tezos.rpc.getBlockHeader({ block: block ? block.toString() : 'head' });
        return Date.parse(header.timestamp);
    }
    
    async function initContract(storageOverrides: any = {}) {
        signWithWallet(Tezos, alice);

        votesContractAddress = (await originateContract(Tezos, 'TokenContractMock.tz', [])).contractAddress!;
        tokenContractAddress = (await originateContract(Tezos, 'TokenContractMock.tz', [])).contractAddress!;

        const storage = {
            token_contract: tokenContractAddress,
            votes_contract: votesContractAddress,
            admin: {
                admin: alice.publicKeyHash,
                pending_admin: null,
                paused: false
            },
            pool_size: 1,
            last_accrued_rewards_at: 1678223768,
            reward_rate: 0,
            votes_minted: 1,
            metadata: new MichelsonMap(),
            ...storageOverrides,
        };
    
        const originationOp = await originateContract(Tezos, 'GovernancePool.tz', storage);
        contractAddress = originationOp.contractAddress!;
    }

    async function getContract(address?: string) {
        return await Tezos.contract.at(address ?? contractAddress);
    }

    async function callEntrypoint(call: ContractMethod<ContractProvider>, sendParams?: SendParams): Promise<TransactionOperation> {
        let op = await call.send({ ...defaultSendParams, ...sendParams });
        await op.confirmation();
        return op;
    }

    async function expectTokenContractTransaction(tokenContractAddress: string, expectedCalls: any[]): Promise<void> {
        const tokenContract = await getContract(tokenContractAddress);

        const tokenContractStorage = await tokenContract.storage<any>();
        const calls = tokenContractStorage.reverse();

        expect(calls).toEqual(expectedCalls);
    }

    async function expectSuccessfulDeposit(by: string, depositAmount: BigNumber, votesAmount: BigNumber): Promise<void> {
        await expectTokenContractTransaction(votesContractAddress, [
            { mint_tokens: [  { owner: contractAddress, amount: votesAmount } ] },
            { transfer: [{
                from_: contractAddress,
                txs: [{ amount: votesAmount, to_: by, token_id: new BigNumber(0) }]
            }]},
        ]);
        await expectTokenContractTransaction(tokenContractAddress, [
            { transfer: [{
                from_: alice.publicKeyHash,
                txs: [{ amount: depositAmount, to_: contractAddress, token_id: new BigNumber(0) }]
            }]},
        ]);
    }

    async function expectSuccessfulWithdrawal(by: string, votesAmount: BigNumber, withdrawAmount: BigNumber): Promise<void> {
        await expectTokenContractTransaction(votesContractAddress, [
            { transfer: [{
                from_: by,
                txs: [{ amount: votesAmount, to_: contractAddress, token_id: new BigNumber(0) }]
            }]},
            { burn_tokens: [  { owner: contractAddress, amount: votesAmount } ] },
        ]);
        await expectTokenContractTransaction(tokenContractAddress, [
            { transfer: [{
                from_: contractAddress,
                txs: [{ amount: withdrawAmount, to_: alice.publicKeyHash, token_id: new BigNumber(0) }]
            }]},
        ]);
    }

    describe('set_admin()', () => {
        test('should fail if tez amount is sent', async () => {
            await initContract();
            const contract = await getContract();
            await expect(callEntrypoint(contract.methods.set_admin(bob.publicKeyHash), { amount: 1 })).rejects.toThrow('AMOUNT_NOT_ZERO');
        });

        test('should succeed when called by admin', async () => {
            await initContract();
            const contract = await getContract();
            await callEntrypoint(contract.methods.set_admin(bob.publicKeyHash));
            const storage = await contract.storage<any>();
            expect(storage.admin.pending_admin).toEqual(bob.publicKeyHash);
        });

        test('should succeed when called by admin when paused', async () => {
            await initContract({ admin: { admin: alice.publicKeyHash, pending_admin: undefined, paused: true } });
            const contract = await getContract();
            await callEntrypoint(contract.methods.set_admin(bob.publicKeyHash));
            const storage = await contract.storage<any>();
            expect(storage.admin.pending_admin).toEqual(bob.publicKeyHash);
        });

        test('should fail when called by non-admin', async () => {
            await initContract();
            const contract = await getContract();
            signWithWallet(Tezos, bob);
            await expect(callEntrypoint(contract.methods.set_admin(jane.publicKeyHash))).rejects.toThrow('NOT_AN_ADMIN');
        });
    });

    describe('confirm_admin()', () => {
        test('should fail if tez amount is sent', async () => {
            await initContract();
            const contract = await getContract();
            await expect(callEntrypoint(contract.methods.confirm_admin(), { amount: 1 })).rejects.toThrow('AMOUNT_NOT_ZERO');
        });

        test('should succeed when called by pending admin', async () => {
            await initContract({ admin: { admin: alice.publicKeyHash, pending_admin: bob.publicKeyHash, paused: false } });
            const contract = await getContract();
            signWithWallet(Tezos, bob);
            await callEntrypoint(contract.methods.confirm_admin());
            const storage = await contract.storage<any>();
            expect(storage.admin.admin).toEqual(bob.publicKeyHash);
            expect(storage.admin.pending_admin).toBeNull();
        });

        test('should succeed when called by pending admin when paused', async () => {
            await initContract({ admin: { admin: alice.publicKeyHash, pending_admin: bob.publicKeyHash, paused: true } });
            const contract = await getContract();
            signWithWallet(Tezos, bob);
            await callEntrypoint(contract.methods.confirm_admin());
            const storage = await contract.storage<any>();
            expect(storage.admin.admin).toEqual(bob.publicKeyHash);
            expect(storage.admin.pending_admin).toBeNull();
        });

        test('should fail when called by anybody else', async () => {
            await initContract({ admin: { admin: alice.publicKeyHash, pending_admin: bob.publicKeyHash, paused: true } });
            const contract = await getContract();
            await expect(callEntrypoint(contract.methods.confirm_admin())).rejects.toThrow('NOT_A_PENDING_ADMIN');
        });

        test('should fail when no pending admin', async () => {
            await initContract();
            const contract = await getContract();
            await expect(callEntrypoint(contract.methods.confirm_admin())).rejects.toThrow('NO_PENDING_ADMIN');
        });
    });

    describe('pause()', () => {
        test('should fail if tez amount is sent', async () => {
            await initContract();
            const contract = await getContract();
            await expect(callEntrypoint(contract.methods.pause(true), { amount: 1 })).rejects.toThrow('AMOUNT_NOT_ZERO');
        });

        test('should pause when called by admin', async () => {
            await initContract();
            const contract = await getContract();
            await callEntrypoint(contract.methods.pause(true));
            const storage = await contract.storage<any>();
            expect(storage.admin.paused).toEqual(true);
        });

        test('should unpause when called by admin', async () => {
            await initContract({ admin: { admin: alice.publicKeyHash, pending_admin: null, paused: true } });
            const contract = await getContract();
            await callEntrypoint(contract.methods.pause(false));
            const storage = await contract.storage<any>();
            expect(storage.admin.paused).toEqual(false);
        });

        test('should fail when called by non-admin', async () => {
            const contract = await getContract();
            signWithWallet(Tezos, bob);
            await expect(callEntrypoint(contract.methods.pause(true))).rejects.toThrow('NOT_AN_ADMIN');
        });
    });

    describe('change_reward_rate()', () => {
        beforeEach(async () => {
            await initContract();
        });

        test('should fail if tez amount is sent', async () => {
            const contract = await getContract();
            await expect(callEntrypoint(contract.methods.change_reward_rate(100), { amount: 1 })).rejects.toThrow('AMOUNT_NOT_ZERO');
        });

        test('should succeed when called by admin', async () => {
            const contract = await getContract();
            await callEntrypoint(contract.methods.change_reward_rate(100));
            const storage = await contract.storage<any>();
            expect(storage.reward_rate).toEqual(new BigNumber(100));
        });

        test('should fail when called by non-admin', async () => {
            const contract = await getContract();
            signWithWallet(Tezos, bob);
            await expect(callEntrypoint(contract.methods.change_reward_rate(100))).rejects.toThrow('NOT_AN_ADMIN');
        });
    });

    describe('deposit()', () => {
        test('should fail if tez amount is sent', async () => {
            await initContract();
            const contract = await getContract();
            await expect(callEntrypoint(contract.methods.deposit(10), { amount: 1 })).rejects.toThrow('AMOUNT_NOT_ZERO');
        });

        test('should fail when paused', async () => {
            await initContract({ admin: { admin: alice.publicKeyHash, pending_admin: null, paused: true } });
            const contract = await getContract();
            await expect(callEntrypoint(contract.methods.deposit(0))).rejects.toThrow('PAUSED');
        });
        
        test('should succeed with empty deposit', async () => {
            await initContract();
            const contract = await getContract();
            await callEntrypoint(contract.methods.deposit(0));
            await expectSuccessfulDeposit(alice.publicKeyHash, new BigNumber(0), new BigNumber(0));
        });
        
        test('should succeed with non-empty deposit and no rewards in the pool', async () => {
            await initContract({ pool_size: 1000, votes_minted: 500 });
            const contract = await getContract();
            await callEntrypoint(contract.methods.deposit(500));
            await expectSuccessfulDeposit(alice.publicKeyHash, new BigNumber(500), new BigNumber(250));
            
            // check storage change
            const storage = await contract.storage<any>();
            expect(storage.pool_size).toEqual(new BigNumber(1500));
            expect(storage.votes_minted).toEqual(new BigNumber(750));
        });

        test('should succeed with non-empty deposit and calculated rewards in the pool', async () => {
            const poolVotesMinted = 500;
            const poolSize = 1000;

            // init contract
            await initContract({ pool_size: poolSize, votes_minted: poolVotesMinted });
            const contract = await getContract();

            // enable rewards (2 tokens per second)
            const rewardRatePerSecond = 2;
            await callEntrypoint(contract.methods.change_reward_rate(rewardRatePerSecond * 1_000_000_000));
            const storage = await contract.storage<any>();
            const lastAccruedRewardsAt = Date.parse(storage.last_accrued_rewards_at);
            
            // deposit 500 tokens
            const depositedAmount = 500;
            await callEntrypoint(contract.methods.deposit(depositedAmount));
            
            // check deposit on token contract
            const expectedRewards = rewardRatePerSecond * (await getTezosTime() - lastAccruedRewardsAt) / 1000;
            const expectedVotesAmount = new BigNumber(depositedAmount).times(poolVotesMinted).dividedToIntegerBy(poolSize + expectedRewards);
            await expectSuccessfulDeposit(alice.publicKeyHash, new BigNumber(depositedAmount), expectedVotesAmount);
            
            // check storage change
            const storageAfterDeposit = await contract.storage<any>();
            expect(storageAfterDeposit.pool_size).toEqual(new BigNumber(1500));
            expect(storageAfterDeposit.votes_minted).toEqual(new BigNumber(500).plus(expectedVotesAmount));
        });
    });

    describe('withdraw()', () => {
        test('should fail if tez amount is sent', async () => {
            await initContract();
            const contract = await getContract();
            await expect(callEntrypoint(contract.methods.withdraw(10), { amount: 1 })).rejects.toThrow('AMOUNT_NOT_ZERO');
        });

        test('should fail when paused', async () => {
            await initContract({ admin: { admin: alice.publicKeyHash, pending_admin: null, paused: true } });
            const contract = await getContract();
            await expect(callEntrypoint(contract.methods.withdraw(0))).rejects.toThrow('PAUSED');
        });
        
        test('should succeed with empty amount', async () => {
            await initContract();
            const contract = await getContract();
            await callEntrypoint(contract.methods.withdraw(0));
            await expectSuccessfulWithdrawal(alice.publicKeyHash, new BigNumber(0), new BigNumber(0));
        });
        
        test('should succeed with non-empty amount and no rewards in the pool', async () => {
            await initContract({ pool_size: 1000, votes_minted: 500 });
            const contract = await getContract();
            await callEntrypoint(contract.methods.withdraw(250));
            await expectSuccessfulWithdrawal(alice.publicKeyHash, new BigNumber(250), new BigNumber(500));

            // check storage change
            const storage = await contract.storage<any>();
            expect(storage.pool_size).toEqual(new BigNumber(500));
            expect(storage.votes_minted).toEqual(new BigNumber(250));
        });

        test('should succeed with non-empty amount and calculated rewards in the pool', async () => {
            const poolVotesMinted = 500;
            const poolSize = 1000;

            await initContract({ pool_size: poolSize, votes_minted: poolVotesMinted });
            const contract = await getContract();

            // enable rewards (2 tokens per second)
            const rewardRatePerSecond = 2;
            await callEntrypoint(contract.methods.change_reward_rate(rewardRatePerSecond * 1_000_000_000));
            const storage = await contract.storage<any>();
            const lastAccruedRewardsAt = Date.parse(storage.last_accrued_rewards_at);
            
            // withdraw tokens corresponding to 250 votes
            const votesReturned = 250;
            await callEntrypoint(contract.methods.withdraw(votesReturned));
            
            // check withdrawal on token contract
            const expectedRewards = rewardRatePerSecond * (await getTezosTime() - lastAccruedRewardsAt) / 1000;
            const expectedTokenAmount = new BigNumber(votesReturned).times(poolSize + expectedRewards).dividedToIntegerBy(poolVotesMinted);
            await expectSuccessfulWithdrawal(alice.publicKeyHash, new BigNumber(votesReturned), expectedTokenAmount);
            
            // check storage change
            const storageAfterWithdrawal = await contract.storage<any>();
            expect(storageAfterWithdrawal.pool_size).toEqual(new BigNumber(poolSize).plus(expectedRewards).minus(expectedTokenAmount));
            expect(storageAfterWithdrawal.votes_minted).toEqual(new BigNumber(poolVotesMinted - votesReturned));
        });

        test('should succeed withdrawing everything including calculated rewards in the pool', async () => {
            const poolVotesMinted = 1001;
            const poolSize = 1001;

            await initContract({ pool_size: poolSize, votes_minted: poolVotesMinted });
            const contract = await getContract();

            // enable rewards (2 tokens per second)
            const rewardRatePerSecond = 2;
            await callEntrypoint(contract.methods.change_reward_rate(rewardRatePerSecond * 1_000_000_000));
            const storage = await contract.storage<any>();
            const lastAccruedRewardsAt = Date.parse(storage.last_accrued_rewards_at);
            
            // return all votes
            const votesReturned = 1000;
            await callEntrypoint(contract.methods.withdraw(votesReturned));
            
            // check withdrawal on token contract
            const expectedRewards = rewardRatePerSecond * (await getTezosTime() - lastAccruedRewardsAt) / 1000;
            const expectedTokenAmount = new BigNumber(votesReturned).times(poolSize + expectedRewards).dividedToIntegerBy(poolVotesMinted);
            await expectSuccessfulWithdrawal(alice.publicKeyHash, new BigNumber(votesReturned), expectedTokenAmount);
            
            // check storage change
            const storageAfterWithdrawal = await contract.storage<any>();
            expect(storageAfterWithdrawal.pool_size).toEqual(new BigNumber(poolSize).plus(expectedRewards).minus(expectedTokenAmount));
            expect(storageAfterWithdrawal.votes_minted).toEqual(new BigNumber(1));
        });
    });
});

