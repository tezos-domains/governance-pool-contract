import { ContractAbstraction, ContractProvider, SendParams } from "@taquito/taquito";
import BigNumber from "bignumber.js";

export const sendParams: SendParams = {
    amount: 0,
    storageLimit: 1_000
};

