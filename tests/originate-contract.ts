import { InMemorySigner } from "@taquito/signer";
import { OriginationOperation, DefaultContractType, TezosToolkit, MichelsonMap } from "@taquito/taquito";
import config from '../.taq/config.json';
import { getTaqueriaConfig, TaqWallet } from "./taqueria-config";

export async function originateContract(tezosToolkit: TezosToolkit, michelsonSourceFile: string, storage: any): Promise<OriginationOperation<DefaultContractType>> {
    const { readFile } = require('fs/promises');
    const { join } = require('path');
    const contractFile = join(config.artifactsDir, michelsonSourceFile);

    const originationOp = await tezosToolkit.contract.originate({
        code: await readFile(contractFile, { encoding: 'utf8' }),
        storage,
    });
    await originationOp.confirmation();
    return originationOp;
}


export function signWithWallet(tezosToolkit: TezosToolkit, wallet: TaqWallet) {
    const signer = new InMemorySigner(secretKey(wallet));
    tezosToolkit.setSignerProvider(signer);
}

export function secretKey(wallet: TaqWallet): string {
    return wallet.secretKey.replace(/unencrypted:/, '');
}
