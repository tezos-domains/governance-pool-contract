export interface TaqWallet {
	encryptedKey: string;
	publicKeyHash: string;
	secretKey: string;
};

export interface TaqConfig {
	rpcUrl: string;
	accounts: {
		alice: TaqWallet;
		bob: TaqWallet;
		jane: TaqWallet;
	};
}

export const getTaqueriaConfig = (): TaqConfig => {

	const env = process.env["CI_ENV"] || "development"
	const config = require(`../.taq/config.local.${env}.json`);
	const rpcUrl = config.rpcUrl;
	const { alice, bob, jane } = config.accounts;

	return {
		rpcUrl,
		accounts: { alice, bob, jane }
	}
}
