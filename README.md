# Tezos Domains Governance Pool

A contract implementing a pool of deposited tokens with linearly accumulating rewards.

## Basic Function
The contract allows users to deposit an amount of a pre-configured primary token (or just "token") in exchange for an amount of a pre-configured vote token (or "votes").
Upon deposit, the pool retains the deposited balance of tokens and mints a corresponding amount of votes, transferring them to the user.  By keeping tokens in the pool, users automatically earn linearly distributed rewards, which are claimed upon withdrawal and returning of votes.

The exchange rate of tokens to votes (or vice versa) can be calculated using the formula `tokens = votes * total_pool_size() / votes_minted`, where:
- `total_pool_size()` represents the current total pool size, consisting of the deposited tokens and accumulated rewards for the entire pool
- `votes_minted` is the total amount of votes currently in circulation

This means that votes represent the total share of the pool each user holds. It is essential that only this contract has the ability to mint new votes.

## Total Pool Size Calculation
Initially, the pool is allocated an additional balance of tokens that are distributed as rewards over time. The total pool size, including rewards, is calculated as:

```
total_pool_size() = pool_size + pending_rewards()
```

In this formula:
- `pool_size` represents the current pool size, including deposited tokens and accrued rewards
- `pending_rewards()` refers to the rewards that have not yet been added to the pool and can be calculated

## Pending rewards and Rewards Accrual

```
pending_rewards() = (NOW() - last_accrued_rewards_at) * rewards_rate
```

Where:
- `NOW()` is the current epoch time in seconds (typically the block timestamp when executed)
- `last_accrued_rewards_at` is the epoch time of the last rewards accrual 
- `rewards_rate` is the rate at which rewards are added to the pool per second

On each withdrawal or change in the `rewards_rate`, the current pending rewards are accrued, meaning that:
- `pool_size` is increased by `pending_rewards()`
- `last_accrued_rewards_at` is updated to the current timestamp (i.e. timestamp of the containing block), effectively resetting `pending_rewards()` value to zero

It is assumed that the contract always holds enough token balance to cover all of the current deposits including corresponding rewards. This naturally requires active management over time (changing the reward rate, transferring more tokens to the pool, or both).

## Notes on Arithmetics
All calculations are performed with natural number arithmetics with divisions always rounded down. As a result, there may be minor rounding errors during each deposit and withdrawal. These errors are considered negligible for token decimal precisions of 1-e6 or higher. The rounding error benefits the pool on both deposit and withdrawal, which mitigates the theoretical risk of the pool underflowing.

Also note that the `rewards_rate` has an added precision of 1e-9 to allow for precise daily/monthly/yearly reward amounts.

## Entrypoints

All of the entrypoints disallow sending non-zero Tezos amounts.

### `deposit`

The entrypoint allows depositing tokens and giving votes in return, using the current tokens-to-votes exchange rate.

The actions performed are:
1. Transferring specified amount of tokens on behalf of the sender to this contract. For this to succeed, the following must be true:
    - This contract is an operator of the sender on the token contract (typically batched together by a UI, with removal of the permission immediately following).
    - The sender has sufficient token balance.
2. Minting corresponding amount of votes (assumes this contract has minting permission on the vote contract).
3. Transferring the votes to the sender.

#### Parameters:
- `nat`: the only parameter of the entrypoint specifies the token amount to deposit. Calling the entrypoint with `0` amount is permitted. 

### `withdraw`

The entrypoint allows returning votes and withdrawing tokens, using the current votes-to-token exchange rate.

The actions performed are:
1. Transferring specified amount of votes on behalf of the sender to this contract. For this to succeed, the following must be true:
    - This contract is an operator of the sender on the votes contract (typically batched together by a UI, with removal of the permission immediately following).
    - The sender has sufficient vote balance.
2. Burning the transferred votes (assumes this contract has burning permission on the vote contract).
3. Transferring the corresponding amount of tokens to the sender.

#### Parameters:
- `nat`: the only parameter of the entrypoint specifies the amount of votes to return. Calling the entrypoint with `0` amount is permitted.

### `change_reward_rate`

The entrypoint calculates the current pending rewards, adds them to the pool and sets a new reward rate. This entrypoint can only be called by the current admin.

#### Parameters:
- `nat`: the new reward rate in 1/10^9s per second (example: to add 2 whole tokens per second, with the precision of the token being 1e-6, the reward rate should be 2e15). Rate of `0` stops the addition of rewards completely.

### `set_admin`

The entrypoint sets a new pending admin to be confirmed. It can only be called by the current admin.

#### Parameters:
- `address`: the new pending admin

### `confirm_admin`

The entrypoint confirms the current pending admin as the new admin. It can only be called by the current pending admin.

### `pause`

Calling with `true`/`false` can be used by the current admin to pause/unpause the `deposit` and `withdraw` entrypoints.

#### Parameters:
- `bool`: the new paused status

## Dependencies

This project requires [Taqueria](https://taqueria.io/) and [NodeJS](https://nodejs.org/).

## Installation and Running The Tests

### Install dependencies

`npm install`

### Start Tezos Sandbox

`taq start sandbox`

When running for the first time, it will download sandbox docker image.
It may take a few seconds until sandbox is bootstrapped.

### Run The Tests

`npm run test`

## Compiling contracts
The contracts can be compiled using `taq compile`:

`taq compile GovernancePool.mligo`

## Deployment on Ghostnet

### Deployment

`taq deploy GovernancePool --env testing --storage <your_storage_file>`

Note that Taqueria will initially generate an account and ask you to fund it on testnet (when ran for the first time).

